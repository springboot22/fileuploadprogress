package com.example.sfp.config;

import com.example.sfp.util.MyCache;
import com.example.sfp.util.Progress;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.ProgressListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FileUploadProgressListener implements ProgressListener {

    private long taskId;

    @Autowired
    MyCache myCache;

    /**
     * 进度大于某个百分比跨度就刷新缓存的进度记录
     */
    private static final int PROGRESS_REFRESH = 10;

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    @Override
    public void update(long pBytesRead, long pContentLength, int pItems) {
        doUpdate(pBytesRead, pContentLength, pItems);
    }

    @Async
    public void doUpdate(long pBytesRead, long pContentLength, int pItems) {
        Progress status = (Progress) myCache.getCacheObject(Progress.genProgressKey(taskId));
        if (status == null) {
            status = new Progress();
        }
        long bafenbi = (pBytesRead * 100) / pContentLength;
        if (bafenbi - status.getBafenbi() < PROGRESS_REFRESH) {
            return;
        }
        status.setpBytesRead(pBytesRead);
        status.setpContentLength(pContentLength);
        status.setpItems(pItems);
        status.setBafenbi(bafenbi);
        myCache.setCacheObject(Progress.genProgressKey(taskId), status);
        log.info("FileUploadProgressListener>:{}", status);
    }

}
