package com.example.sfp.util;

import com.gms.common.constant.Constants;

public class Progress {

    private static final ThreadLocal<Long> TASK_ID = new ThreadLocal<>();

    public static void setTaskId(long taskId) {
        TASK_ID.set(taskId);
    }

    public static long getTaskId() {
        return TASK_ID.get();
    }

    public static String genProgressKey(long taskId) {
        return Constants.NOTE_FILE_UPLOAD_KEY_PREFIX + taskId;
    }

    private long pBytesRead;
    private long pContentLength;
    private long pItems;

    private double bafenbi;

    public long getpBytesRead() {
        return pBytesRead;
    }

    public void setpBytesRead(long pBytesRead) {
        this.pBytesRead = pBytesRead;
    }

    public long getpContentLength() {
        return pContentLength;
    }

    public void setpContentLength(long pContentLength) {
        this.pContentLength = pContentLength;
    }

    public long getpItems() {
        return pItems;
    }

    public void setpItems(long pItems) {
        this.pItems = pItems;
    }

    public double getBafenbi() {
        return bafenbi;
    }

    public void setBafenbi(double bafenbi) {
        this.bafenbi = bafenbi;
    }

    @Override
    public String toString() {
        return "Progress [pBytesRead=" + pBytesRead + ", pContentLength=" + pContentLength + ", pItems=" + pItems
                + ", baifenbi=" + bafenbi + "]";
    }

}
