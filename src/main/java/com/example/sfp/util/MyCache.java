package com.example.sfp.util;

import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Component;

/**
 * @author will.tuo
 * @date 2022/8/8 16:35
 */
@Component
public class MyCache {

    private final ConcurrentHashMap<String, Object> cache = new ConcurrentHashMap<>();

    public Object getCacheObject(String key) {
        return cache.get(key);
    }

    public void setCacheObject(String key, Object obj) {
        cache.put(key, obj);
    }
}
