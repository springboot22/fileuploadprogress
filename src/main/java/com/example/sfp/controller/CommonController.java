package com.example.sfp.controller;

import com.example.sfp.util.Progress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 通用请求处理
 *
 * @author ruoyi
 */
@RestController
public class CommonController {

    private static final Logger log = LoggerFactory.getLogger(CommonController.class);
    private String frontEndBaseUri;


    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    public Object uploadFile(MultipartFile file,
            @RequestParam(required = false, defaultValue = "") String prefixPath, HttpServletRequest request)
            throws Exception {
        Long taskId = Progress.getTaskId();
        try {
            // 上传文件路径
            String filePath = "";
            List<Map<String, String>> fileResultList = new ArrayList<>();
            Map<String, String> fileResultMap = new HashMap<>();
            // 上传并返回新文件名称
            long start = System.currentTimeMillis();
//            String fileName = FileUploadUtils.upload(filePath, file);
            long end = System.currentTimeMillis();
            log.info("时间：{}", end - start);
//            String url = serverConfig.getUrl() + fileName;
//            fileResultMap.put("url", url);
//            fileResultMap.put("fileName", fileName);
            fileResultMap.put("fileRealName", file.getOriginalFilename());
            fileResultMap.put("fileName1", file.getName());
            fileResultMap.put("contentType", file.getContentType());
//            fileResultMap.put("extension", FileUploadUtils.getExtension(file));
            fileResultList.add(fileResultMap);
            Map<String, Object> success = new HashMap<>();
            success.put("taskId", taskId);
            success.put("time", end - start);
            return success;
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    
}
