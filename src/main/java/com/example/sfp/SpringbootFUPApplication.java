package com.example.sfp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootFUPApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootFUPApplication.class, args);
    }

}
